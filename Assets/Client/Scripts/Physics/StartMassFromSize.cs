﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMassFromSize : MonoBehaviour
{
    public float mass_multiplier = 10f;
    // Start is called before the first frame update
    void Start()
    {
        //Для астеройдов немного рандомизируем размеры
        if (gameObject.tag == "Asteroid")
        {
            //Задаем коэфицент рандомизации +-10%
            float random_size = Random.Range(0.9f, 1.1f);
            this.gameObject.transform.localScale = this.gameObject.transform.localScale * random_size;
        }
        //Указываем массу в соответствии с физическими размерами и множителем массы
        gameObject.GetComponent<Rigidbody>().mass = gameObject.GetComponent<MeshRenderer>().bounds.extents.magnitude * mass_multiplier;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
