﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GamePause : MonoBehaviour
{
    public string MenuSceneName;
    public GameObject PauseUILayer, MusicObj, TouchLayer;
    public Text MusicSwitcher;
    public bool paused = false;


    // Start is called before the first frame update
    void Start()
    {
        //Выключаем слой с паузой, если он был включен
        if (PauseUILayer != null) PauseUILayer.SetActive(false);
        //Включаем слой с тачем, если в глобальной переменной было указано, что он нужен
        if ((PlayerPrefs.GetInt("TouchLayer") == 1) && (TouchLayer != null)) TouchLayer.SetActive(true);
        //Гасим музыку, если она была ранее отключена и сейчас активна
        if ((PlayerPrefs.GetInt("MusicMute") == 1) && (MusicObj.activeSelf == true)) SwitchMusic();
    }

    // Update is called once per frame
    void Update()
    {
        //Проверяем нажатие кнопки "пауза"
        if (Input.GetButtonDown("Cancel"))
        {
            if (paused == false) PauseGame(); 
            else ResumeGame(); 
        }
    }

    public void ResumeGame()
    {
        paused = false;
        Time.timeScale = 1;
        //Деактивируем слой с кнопками паузы
        PauseUILayer.SetActive(false);
        //Включаем слой с тачем, если в глобальной переменной было указано, что он нужен
        if ((PlayerPrefs.GetInt("TouchLayer") == 1) && (TouchLayer != null)) TouchLayer.SetActive(true);
        //Задаем период обновления физики
        Time.fixedDeltaTime = 1 / (50 / Time.timeScale);
    }

    public void PauseGame()
    {
        paused = true;
        Time.timeScale = 0.001f;
        //Активируем слой с кнопками паузы если он задан
        if (PauseUILayer != null) PauseUILayer.SetActive(true);
        //Обязательно деактивируем слой тача
        TouchLayer.SetActive(false);
        //Задаем период обновления физики
        Time.fixedDeltaTime = 1 / (50 / Time.timeScale);
    }

    public void LoadMainMenu()
    {
        ResumeGame();
        SceneManager.LoadScene(MenuSceneName);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Classic Game");
    }

    public void SwitchMusic()
    {
        if (MusicObj.activeSelf == true)
        {
            MusicObj.SetActive(false);
            MusicSwitcher.text = "Music: OFF";
            PlayerPrefs.SetInt("MusicMute", 1);
        }
        else
        {
            MusicObj.SetActive(true);
            MusicSwitcher.text = "Music: ON";
            PlayerPrefs.SetInt("MusicMute", 0);
        }
    }
}
