﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipSelectionState : MonoBehaviour
{
    public GameObject ShipSelectionCamera, StartMenuCamera;
    public GameObject ShipSelectionManagerObj;
    public GameObject ShipSelectionUI, StartMenuUI;

    // Start is called before the first frame update
    void Start()
    {
        StartMenuVoid();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Включаем компоненты для отображения выбора корабля и отключаем стартовый экран
    public void ShipSelectionVoid()
    {
        StartMenuCamera.SetActive(false);
        ShipSelectionCamera.SetActive(true);
        ShipSelectionManagerObj.SetActive(true);
        ShipSelectionUI.SetActive(true);
        StartMenuUI.SetActive(false);
    }

    //Включаем компоненты для стартового экрана и отключаем отображение выбора корабля    
    public void StartMenuVoid()
    {
        ShipSelectionCamera.SetActive(false);
        StartMenuCamera.SetActive(true);
        ShipSelectionManagerObj.SetActive(false);
        ShipSelectionUI.SetActive(false);
        StartMenuUI.SetActive(true);
    }
}
