﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoading : MonoBehaviour
{
    public string SceneName;
    public bool AutoLoad = false;
    public Slider LoadingSlider;
    public List<Button> Buttons;
    int loading_is_start = 60;

    IEnumerator AsyncLoad()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(SceneName);
        while (operation.isDone != true)
        {
            float progress = operation.progress / 0.9f;
            LoadingSlider.value = progress;
            yield return null;
        }
    }

    // Update is called once per frame
    public void Start()
    {
        //StartCoroutine(AsyncLoad());
    }

    public void Update()
    {
        if (loading_is_start > 0) loading_is_start--;
        if ((loading_is_start == 0) && (AutoLoad == true))
        {
            Start_Load_Scene();
        }
    }

    // Update is called once per frame
    public void Start_Load_Scene()
    {
        //Проверка наличия тачскрина
        if (Input.touchCount > 0) PlayerPrefs.SetInt("TouchLayer", 1);
        else PlayerPrefs.SetInt("TouchLayer", 0);
        //Загрузка сцены
        LoadingSlider.enabled = false;
        for (int i = 0; i < Buttons.Count; i++) 
        {
            Buttons[i].enabled = false;
        }
        StartCoroutine(AsyncLoad());
    }

    public void AppExit()
    {
        Application.Quit();
    }
}
