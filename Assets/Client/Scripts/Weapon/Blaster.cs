﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blaster : MonoBehaviour
{
    public GameObject bullet_prefab;
    public GameObject Ship;
    float cooldown = 0;
    public float bullet_speed = 50;
    public float time_between_fire = 0.1f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void Shooting()
    {
        GameObject bullet = Instantiate(bullet_prefab, transform.position, transform.rotation); //Создаём клон объекта с собственными координатами и поворотом
        bullet.GetComponent<Rigidbody>().velocity = Ship.GetComponent<Rigidbody>().velocity;
        bullet.GetComponent<Rigidbody>().AddForce(bullet.transform.forward * bullet_speed, ForceMode.VelocityChange);
        if (bullet_speed / 250 > 0.1) Ship.GetComponent<Rigidbody>().AddForce(bullet.transform.forward * -1 * (bullet_speed / 250), ForceMode.VelocityChange);
        //bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * bullet_speed;
    }

    void FixedUpdate()
    {
        //Прервывание между выстрелами
        if (cooldown > 0) cooldown -= 1;
        //Стрельба
        if ((Input.GetButton("Jump")) && (Time.timeScale == 1))
        {
            if (cooldown <= 0)
            {
                Shooting();
                cooldown = time_between_fire; //0.1f
            }
        }
    }
}
