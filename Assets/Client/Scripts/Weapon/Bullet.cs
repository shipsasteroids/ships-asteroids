﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float lifetime = 5f;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(this.gameObject, lifetime);
    }

    // Update is called once per frame
    void OnCollisionEnter(Collision other)
    {
        //При встрече с астеройдом уничтожаем пулю
        if (other.gameObject.tag == "Asteroid")
        {
            Destroy(this.gameObject, 0);
        }
        //При встрече с драгоценным камнем, отражаем луч в произвольном направлении
        if (other.gameObject.tag == "Jewel")
        {
            //GameObject bullet_reflect = Instantiate(this.gameObject, transform.position, transform.rotation); //Создаём клон объекта с собственными координатами и поворотом
            //bullet_reflect.transform.eulerAngles = new Vector3(0, Random.Range(-180, 180), 0);
            //bullet_reflect.GetComponent<Rigidbody>().velocity = bullet_reflect.transform.forward * this.gameObject.GetComponent<Rigidbody>().velocity.magnitude;
            Destroy(this.gameObject, 0);
        }
    }
}
