﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Classic_Game_Manager : MonoBehaviour
{
    public GameObject BigAsteroidSpawner, MidAsteroidSpawner, SmallAsteroidSpawner;
    public GameObject[] respawns;
    int skybox_switcher = 0;
    public List<Material> Skyboxes;

    // Start is called before the first frame update
    void Start()
    {
        //Заменить скайбокс на нужный
        DynamicGI.UpdateEnvironment();
        //Считываем номер скайбокса и проверяем, чтобы он не повторялся при рандоме
        var prev_skybox_switcher = PlayerPrefs.GetInt("skybox");
        skybox_switcher = Random.Range(0, Skyboxes.Count - 1);
        while (prev_skybox_switcher == skybox_switcher) { skybox_switcher = Random.Range(0, Skyboxes.Count - 1); }
        RenderSettings.skybox = Skyboxes[skybox_switcher];
        PlayerPrefs.SetInt("skybox", skybox_switcher);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        respawns = GameObject.FindGameObjectsWithTag("Asteroid");
        if (respawns.Length == 0)
            {
            //Создаём на один астеройд больше в каждый новый уровень
            ++BigAsteroidSpawner.GetComponent<Classic_Asteroid_Spawner>().Spawn_Count;
            BigAsteroidSpawner.GetComponent<Classic_Asteroid_Spawner>().StartSpawn();
            //++MidAsteroidSpawner.GetComponent<Classic_Asteroid_Spawner>().Spawn_Count;
            //MidAsteroidSpawner.GetComponent<Classic_Asteroid_Spawner>().StartSpawn();
            //++SmallAsteroidSpawner.GetComponent<Classic_Asteroid_Spawner>().Spawn_Count;
            //SmallAsteroidSpawner.GetComponent<Classic_Asteroid_Spawner>().StartSpawn();
            //Обнуляем позицию игрока в каждом новом уровне
            //GameObject player = GameObject.FindGameObjectWithTag("Player");
            //player.transform.position = new Vector3(0, 0, 0);
            //player.GetComponent<Classic_Ship_Control>().body_rotation = 0;
            //player.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        }
    }
}
