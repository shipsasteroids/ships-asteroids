﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Field_Ratio : MonoBehaviour
{
    public GameObject MainCanvas; 
    public float resolution_ratio_height = 20;
    public float resolution_ratio_width = 20;
    public float teleport_zone = 18;
    public GameObject MainCamera;
    public List<GameObject> TeleportPointsHorizontalTop, TeleportPointsHorizontalBottom, TeleportPointsVerticalLeft, TeleportPointsVerticalRight;

    // Start is called before the first frame update
    void Start()
    {
        float width = MainCanvas.GetComponent<RectTransform>().rect.width;
        float height = MainCanvas.GetComponent<RectTransform>().rect.height;
        int TeleportPointsCouter = 0;

        //Если ориентация экрана горизонтальная, то фиксируем высоту поля, в противном случае ширину поля
        if (width >= height)
        {
            resolution_ratio_height = 20; //Задаём базовую высоту поля
            resolution_ratio_width = 20 * (width / height); //Рассчитываем ширину поля из пропорции экрана
            MainCamera.GetComponent<Camera>().orthographicSize = 100; //Задаём удаление камеры для ландшафтного режима
        }
        else
        {
            resolution_ratio_width = 20; //Задаём базовую ширину поля
            resolution_ratio_height = 20 * (height / width); //Рассчитываем высоту поля из пропорции экрана
            MainCamera.GetComponent<Camera>().orthographicSize = height / width * 100; //Задаём удаление камеры для портретного режима
        }

        //Устанавливаем размеры в соответствии с пропорцией экрана
        this.gameObject.transform.localScale = new Vector3(resolution_ratio_width, 0.1f, resolution_ratio_height);
        //Меняем точки телепорта, чтобы не застревали объекты (рассчитываем положение точки односительно размеров поля, а потом делаем фиксированный отступ 17 юнитов для телепортации объектов)
        while (TeleportPointsCouter < TeleportPointsHorizontalTop.Count)
        {
            TeleportPointsHorizontalTop[TeleportPointsCouter].transform.position = new Vector3(TeleportPointsHorizontalTop[TeleportPointsCouter].transform.position.x, TeleportPointsHorizontalTop[TeleportPointsCouter].transform.position.y, (resolution_ratio_height * 5) - teleport_zone);
            ++TeleportPointsCouter;
        }
        TeleportPointsCouter = 0;
        while (TeleportPointsCouter < TeleportPointsHorizontalBottom.Count)
        {
            TeleportPointsHorizontalBottom[TeleportPointsCouter].transform.position = new Vector3(TeleportPointsHorizontalBottom[TeleportPointsCouter].transform.position.x, TeleportPointsHorizontalBottom[TeleportPointsCouter].transform.position.y, (resolution_ratio_height * -1f * 5) + teleport_zone);
            ++TeleportPointsCouter;
        }
        TeleportPointsCouter = 0;
        while (TeleportPointsCouter < TeleportPointsVerticalLeft.Count)
        {
            TeleportPointsVerticalLeft[TeleportPointsCouter].transform.position = new Vector3((resolution_ratio_width * -1f * 5) + teleport_zone, TeleportPointsVerticalLeft[TeleportPointsCouter].transform.position.y, TeleportPointsVerticalLeft[TeleportPointsCouter].transform.position.z);
            ++TeleportPointsCouter;
        }
        TeleportPointsCouter = 0;
        while (TeleportPointsCouter < TeleportPointsVerticalRight.Count)
        {
            TeleportPointsVerticalRight[TeleportPointsCouter].transform.position = new Vector3((resolution_ratio_width * 5) - teleport_zone, TeleportPointsVerticalRight[TeleportPointsCouter].transform.position.y, TeleportPointsVerticalRight[TeleportPointsCouter].transform.position.z);
            ++TeleportPointsCouter;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
