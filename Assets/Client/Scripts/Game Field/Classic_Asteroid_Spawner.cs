﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Classic_Asteroid_Spawner : MonoBehaviour
{
    public GameObject GameField;
    public List<GameObject> Asteroids;
    public List<GameObject> TeleportTargets;
    public int Spawn_Count = 0;
    GameObject created_obj;
    Rigidbody created_obj_body;

    // Start is called before the first frame update
    public void StartSpawn()
    {
        int Spawn_Counter = 0;
        //Создаем объекты в цикле
        while (Spawn_Counter < Spawn_Count)
        {
            Invoke("CreateObject", 0);
            ++Spawn_Counter;
        }
    }

    // Update is called once per frame
    void CreateObject()
    {
        //Создаем объект случайно выбранный из списка в случайной точке из списка со случайным поворотом
        created_obj = Instantiate(Asteroids[Random.Range(0, Asteroids.Count -1)], TeleportTargets[Random.Range(0, TeleportTargets.Count -1)].transform.position, Random.rotation) as GameObject;
        //Придаём объекту случайное направление и скорость полёта
        created_obj.GetComponent<Rigidbody>().AddForce(created_obj.transform.forward * Random.Range(15, 25), ForceMode.VelocityChange);
    }
}
