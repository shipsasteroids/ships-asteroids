﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Classic_Field : MonoBehaviour
{
    public GameObject TeleportTarget;

    // Start is called before the first frame update
    void Start()
    {
            
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
        //Телепортируем объект, если это не пуля
        if (other.gameObject.tag != "BlasterBullet") other.transform.position = TeleportTarget.transform.Find("TeleportTarget").transform.position;
    }
}
