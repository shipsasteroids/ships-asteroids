﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio_Changer : MonoBehaviour
{
    public List<GameObject> BackgroundMusicList;
    AudioSource audioData;
    GameObject SelectedMusicObj;
    int AudioTracksCounter = -1;

    // Start is called before the first frame update
    void Start()
    {
        audioData = GetComponent<AudioSource>();
        SelectedMusicObj = BackgroundMusicList[Random.Range(0, BackgroundMusicList.Count -1)]; //Выбираем произвольный объект с музыкой из списка
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Делаем проверку закончился ли трек и если закончился, то переключаемся на новый
        if (audioData.isPlaying != true)
        {
            //Переключаемся на следующий трек в группе
            AudioTracksCounter++;
            //Если треки в группе закончились, то переключаемся на другую композицию
            if (AudioTracksCounter > SelectedMusicObj.GetComponent<Audio_Parts_List>().TracksList.Count -1)
            {
                AudioTracksCounter = 0; //Сбрасываем счетчик треков на стартовую позицию
                SelectedMusicObj = BackgroundMusicList[Random.Range(0, BackgroundMusicList.Count -1)]; //Выбираем произвольный объект с музыкой из списка
            }
            //Ставим на воспроизведение выбранный трек
            audioData.clip = SelectedMusicObj.GetComponent<Audio_Parts_List>().TracksList[AudioTracksCounter];
            audioData.Play();
        }
    }
}
