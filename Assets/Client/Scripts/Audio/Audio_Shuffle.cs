﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio_Shuffle : MonoBehaviour
{
    AudioSource audioData;
    public List<AudioClip> TracksList;

    // Start is called before the first frame update
    void Start()
    {
        audioData = GetComponent<AudioSource>();
        //Ставим на воспроизведение выбранный трек
        if (audioData.isPlaying != true)
        {
            audioData.clip = TracksList[Random.Range(0, TracksList.Count - 1)];
            audioData.Play();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
