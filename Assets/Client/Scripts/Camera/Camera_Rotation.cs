﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Rotation : MonoBehaviour
{
    public Transform target;
    public Vector3 offset;
    public float sensitivity = 3; // чувствительность мышки
    public float limit = 80; // ограничение вращения по Y
    public float zoom = 0.25f; // чувствительность при увеличении, колесиком мышки
    public float zoomMax = 10; // макс. увеличение
    public float zoomMin = 3; // мин. увеличение
    private float X, Y;

    // Start is called before the first frame update
    void Start()
    {
        limit = Mathf.Abs(limit);
        if (limit > 90) limit = 90;
        if (limit < 0) limit = 0;
        offset = new Vector3(offset.x, offset.y, -Mathf.Abs(zoomMax));
        transform.position = target.position + offset;
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetButton("Fire1"))
            RotateCamera();
    }

    void RotateCamera()
    {
        //if (Input.GetAxis("Mouse ScrollWheel") > 0) offset.z += zoom;
        //else if (Input.GetAxis("Mouse ScrollWheel") < 0) offset.z -= zoom;
        offset.z = Mathf.Clamp(offset.z, -Mathf.Abs(zoomMax), -Mathf.Abs(zoomMin));

        //X = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivity;
        //Y += Input.GetAxis("Mouse Y") * sensitivity;
        X = transform.localEulerAngles.y + (-1 * sensitivity);
        Y += -1 * sensitivity;
        Y = Mathf.Clamp(Y, -limit, limit);
        transform.localEulerAngles = new Vector3(-Y, X, 0);
        transform.position = transform.localRotation * offset + target.position;
    }
}
