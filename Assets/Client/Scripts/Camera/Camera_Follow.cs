﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Follow : MonoBehaviour
{
    public Vector3 offset;
    public GameObject player;
    public float rotate_angle;

    // Start is called before the first frame update
    void Start()
    {
        transform.Rotate(rotate_angle, transform.rotation.y, transform.rotation.z);
    }

    // Update is called once per frame
    void Update()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        //Добавляем камере высоту и сдвиг назад в зависимости от установленных параметров и размеров объекта
        this.gameObject.transform.position = new Vector3(player.transform.position.x, player.transform.position.y + offset.y, player.transform.position.z - offset.z);
    }
}
