﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Camera_Screenshot : MonoBehaviour
{
    public Camera mainCamera;
    int counter = 1;

    #if UNITY_EDITOR
        bool isEditMode = true;
    #else 
        bool isEditMode = false;
    #endif

    private void Update()
    {   
        //Делаем скрины если в редакторе
        if (Input.GetMouseButtonDown(1) && (isEditMode == true))
        {
            ScreenCapture.CaptureScreenshot("Assets/Screenshots/Sreenshot" + counter.ToString("00") + "_" + mainCamera.pixelWidth + "x" + mainCamera.pixelHeight + "_" + "_SceneID" + SceneManager.GetActiveScene().name + ".png");
            counter++;
        }
    }
}
