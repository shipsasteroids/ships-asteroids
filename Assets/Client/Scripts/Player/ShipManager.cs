﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipManager : MonoBehaviour
{
    public List<GameObject> PlayerShips;
    public int SelectedShip = 0;
    public GameObject BuyButton, SelectButton;

    // Start is called before the first frame update
    void Start()
    {
        //Отключаем другие корабли, если они остались включенными
        int ArrayShipCounter = 0;
        while (ArrayShipCounter < PlayerShips.Count - 1)
        {
            PlayerShips[ArrayShipCounter].SetActive(false);
            ++ArrayShipCounter;
        }
        //Загружаем корабль, который был выбран игроком ранее из сохранённых данных
        SelectedShip = PlayerPrefs.GetInt("PlayerShip");
        PlayerShips[SelectedShip].SetActive(true);

        if ((BuyButton != null) && (SelectButton != null)) CheckShipOwning();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        CheckShipOwning();
    }

    public void SwitchShipRight()
    {
        PlayerShips[SelectedShip].SetActive(false);
        SelectedShip++;
        if (SelectedShip > PlayerShips.Count - 1) SelectedShip = 0;
        PlayerShips[SelectedShip].SetActive(true);

        CheckShipOwning();
    }

    public void SwitchShipLeft()
    {
        PlayerShips[SelectedShip].SetActive(false);
        SelectedShip--;
        if (SelectedShip < 0) SelectedShip = PlayerShips.Count - 1;
        PlayerShips[SelectedShip].SetActive(true);

        CheckShipOwning();
    }

    public void SwitchShipColorDown()
    {
        var SelectedColor = PlayerShips[SelectedShip].GetComponent<MaterialSwitcher>().SelectedColor;
        PlayerShips[SelectedShip].GetComponent<MaterialSwitcher>().ShipVariants[SelectedColor].SetActive(false);
        ++SelectedColor;
        if (SelectedColor > PlayerShips[SelectedShip].GetComponent<MaterialSwitcher>().ShipVariants.Count - 1) SelectedColor = 0;
        PlayerShips[SelectedShip].GetComponent<MaterialSwitcher>().ShipVariants[SelectedColor].SetActive(true);
        PlayerShips[SelectedShip].GetComponent<MaterialSwitcher>().SelectedColor = SelectedColor;
    }

    public void SwitchShipColorUp()
    {
        var SelectedColor = PlayerShips[SelectedShip].GetComponent<MaterialSwitcher>().SelectedColor;
        PlayerShips[SelectedShip].GetComponent<MaterialSwitcher>().ShipVariants[SelectedColor].SetActive(false);
        --SelectedColor;
        if (SelectedColor < 0) SelectedColor = PlayerShips[SelectedShip].GetComponent<MaterialSwitcher>().ShipVariants.Count - 1;
        PlayerShips[SelectedShip].GetComponent<MaterialSwitcher>().ShipVariants[SelectedColor].SetActive(true);
        PlayerShips[SelectedShip].GetComponent<MaterialSwitcher>().SelectedColor = SelectedColor;
    }

    public void SaveSelectedShip()
    {
        //Сохраняем выбранный корабль
        PlayerPrefs.SetInt("PlayerShip", SelectedShip);
        //Сохраняем выбранный цвет корабля
        PlayerPrefs.SetInt(PlayerShips[SelectedShip].name, PlayerShips[SelectedShip].GetComponent<MaterialSwitcher>().SelectedColor);
    }

    public void CheckShipOwning()
    {
        //Проверяем был ли куплен корабль либо корабль самый первый
        if ((PlayerPrefs.GetInt("PlayerShip" + SelectedShip) != 0) || (SelectedShip == 0))
        {
            if (SelectButton != null) SelectButton.SetActive(true);
            if (SelectButton != null) BuyButton.SetActive(false);
        }
        else
        {
            SelectButton.SetActive(false);
            if (SelectButton != null) BuyButton.SetActive(true);
            if (SelectButton != null) BuyButton.transform.Find("Buy Button").GetChild(0).GetComponent<Text>().text = "Buy for $" + (PlayerShips[SelectedShip].GetComponent<ShipStatus>().Price / 1000) + "K";
            //Включаем или выключаем кнопку в зависимости от того, есть ли у пользователя достаточно денег на покупку
            if (PlayerPrefs.GetInt("Jewels") - PlayerShips[SelectedShip].GetComponent<ShipStatus>().Price >= 0)
            { if (SelectButton != null) BuyButton.transform.Find("Buy Button").GetComponent<Button>().interactable = true; }
            else
            { if (SelectButton != null) BuyButton.transform.Find("Buy Button").GetComponent<Button>().interactable = false; }
        }
    }

    public void BuyShip()
    {
        int Price = PlayerShips[SelectedShip].GetComponent<ShipStatus>().Price;
        if (PlayerPrefs.GetInt("Jewels") - Price >= 0)
        {
            //Покупаем корабль
            PlayerPrefs.SetInt("Jewels", (PlayerPrefs.GetInt("Jewels") - Price));
            PlayerPrefs.SetInt("PlayerShip" + SelectedShip, 1);
            CheckShipOwning();
        }
        else
        {
            //Выводим сообщение о нехватке денег
            BuyButton.transform.Find("Buy Button").GetChild(0).GetComponent<Text>().text = "No money for it :(";
        }

    }
}
