﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Classic_Ship_Control : MonoBehaviour
{
    Rigidbody body, RotatePoint;
    float body_rotation = 0f;
    public float body_rotation_speed = 1f;
    public float body_forward_speed = 1f;
    public GameObject EngineRear, EngineLeft, EngineRight;
    public GameObject RotatePointObj;
    GamePause PauseObject;
    Vector3 NormalVector;
    float rotation_y_1, rotation_y_2;
    float v, h;

    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody>();
        RotatePoint = RotatePointObj.GetComponent<Rigidbody>();
        EngineRear.SetActive(false);
        EngineLeft.SetActive(false);
        EngineRight.SetActive(false);
        PauseObject = GameObject.Find("Pause Manager").GetComponent<GamePause>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        v = Input.GetAxis("Vertical"); //-1 вниз, +1 вверх
        //if (Input.GetAxis("Vertical") > 0) v = 1f;
        //if (Input.GetAxis("Vertical") < 0) v = -1f;
        //if (Input.GetAxis("Vertical") == 0) v = 0;
        h = Input.GetAxis("Horizontal"); //-1 влево, +1 вправо
        //if (Input.GetAxis("Horizontal") > 0) h = 1f;
        //if (Input.GetAxis("Horizontal") < 0) h = -1f;
        //if (Input.GetAxis("Horizontal") == 0) h = 0;
        var input_vector = new Vector3(h, 0, v);
        float health = this.gameObject.GetComponent<ShipStatus>().Health;
        //Если есть вращение но мы его не задаём, то включаем стабилизацию
        //цифровой - if ((Input.GetAxis("Horizontal") == 0) && (body.angularVelocity.magnitude > 0.001 + ((100 - health) * 0.005))) //((RotatePoint.angularVelocity.magnitude > 0.001) && (h == 0))
        //if ((Input.GetAxis("Horizontal") == 0) && (body.angularVelocity.magnitude > 0.001)) //((RotatePoint.angularVelocity.magnitude > 0.001) && (h == 0))
        //цифровой - {
        //    rotation_y_2 = body.transform.rotation.eulerAngles.y;
        //    if (rotation_y_1 != rotation_y_2)
        //    {
        //        if (rotation_y_1 > rotation_y_2) h = 5f;
        //        if (rotation_y_1 < rotation_y_2) h = -5f;
        //        if (rotation_y_1 * rotation_y_2 < -0.1f)
        //        {
        //            if (rotation_y_1 > rotation_y_2) h = -5f;
        //            if (rotation_y_1 < rotation_y_2) h = 5f;
        //        }
        //    }
        //    rotation_y_1 = rotation_y_2;
        //}

        if (input_vector.magnitude >= 0.15f)
        {
            //Летим вперед если вектор направления совпадает с заданным вектором и обе оси не в нуле
            if (Vector3.Angle(transform.forward, input_vector) <= 30)
            {
                body.AddForce((transform.forward + input_vector) * (body_forward_speed * (health / 100)), ForceMode.VelocityChange);
                EngineRear.SetActive(true); //Включаем эффекты полёта вперёд
            }
            else EngineRear.SetActive(false); //Выключаем эффекты полёта вперёд
            //Задаём угол поворота корабля
            if (transform.forward != input_vector) transform.forward = transform.forward + (input_vector * (body_rotation_speed / body.mass * (health / 100)));
            if (Vector3.Angle(transform.right, input_vector) > 90) { EngineRight.SetActive(false); EngineLeft.SetActive(true); }
            if (Vector3.Angle(transform.right, input_vector) < 90) { EngineRight.SetActive(true); EngineLeft.SetActive(false); }
            if (Vector3.Angle(transform.right, input_vector) == 180) { EngineRight.SetActive(true); EngineLeft.SetActive(true); }
            if (Vector3.Angle(transform.right, input_vector) == 90) { EngineRight.SetActive(false); EngineLeft.SetActive(false); }
            //Если игрок управляет кораблём, то сбрасываем вращение от физических взаимодействий
            GetComponent<Rigidbody>().angularVelocity = Random.insideUnitSphere * 0;
        }
        else
        {
            EngineRear.SetActive(false); //Выключаем эффекты полёта вперёд
            EngineRight.SetActive(false); EngineLeft.SetActive(false); //Выключаем эффекты поворотов
        }
        //Выравниваем корабль в горизонтальной плоскости
        if (transform.eulerAngles != new Vector3(0, transform.eulerAngles.y, 0)) transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);

        //цифровой - if (v < 0) body.AddForce(transform.forward * (v * body_forward_speed * 0.5f * (health/100)), ForceMode.VelocityChange);
        //Включаем эффекты при поворотах
        //if ((h > 0) || (v < 0))
        //    EngineRight.SetActive(true);
        //else EngineRight.SetActive(false);
        //if ((h < 0) || (v < 0))
        //    EngineLeft.SetActive(true);
        //else EngineLeft.SetActive(false);
        //speed = body.velocity.magnitude;
        //Сбрасываем поворот корабля если ставим игру на паузу 
        if ((PauseObject.paused == true) && (h != 0)) h = 0;
        //if (body_rotation != body_rotation + (h * body_rotation_speed)) body_rotation = body_rotation + (h * body_rotation_speed);
        //if (transform.eulerAngles != new Vector3(0, body_rotation, 0)) transform.eulerAngles = new Vector3(0, body_rotation, 0);
        //цифровой - if (Input.GetAxis("Horizontal") != 0)
        //{
        //цифровой -     if (body.angularVelocity.magnitude < body_rotation_speed / 5) RotatePoint.AddForce(RotatePoint.transform.forward * h * body_rotation_speed * (health / 100), ForceMode.VelocityChange); //VelocityChange);
        //}
        //цифровой - else RotatePoint.AddForce(RotatePoint.transform.forward * h * body_rotation_speed * (health / 100), ForceMode.VelocityChange);
        //transform.Rotate(transform.rotation.x, rotation, transform.rotation.z);
    }

    void Update()
    {
   
        
    }

}
