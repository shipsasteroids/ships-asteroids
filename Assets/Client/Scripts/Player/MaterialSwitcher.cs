﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialSwitcher : MonoBehaviour
{
    public List<GameObject> ShipVariants;
    public int SelectedColor = 0;

    // Start is called before the first frame update
    void Start()
    {
        //Отключаем все варианты цвета корабля перед тем как загрузить какой-то один
        int ArrayColorCounter = 0;
        while (ArrayColorCounter < ShipVariants.Count - 1)
        {
            ShipVariants[ArrayColorCounter].SetActive(false);
            ++ArrayColorCounter;
        }
        //Загружаем цвет корабля, который был выбран игроком ранее из сохранённых данных
        SelectedColor = PlayerPrefs.GetInt(this.gameObject.name);
        ShipVariants[SelectedColor].SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
    
    }
}
