﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jewel_Behaver : MonoBehaviour
{
    public int Jewel_Cost;
    public GameObject Explode_prefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void OnCollisionEnter(Collision other)
    {
        //При встрече с игроком передаём свою ценность игроку и исчезаем
        if (other.gameObject.tag == "Player")
        {
            PlayerPrefs.SetInt("Jewels", PlayerPrefs.GetInt("Jewels") + Jewel_Cost);
            Destroy(this.gameObject, 0);
        }
        //При встрече с пулей бластера
        if (other.gameObject.tag == "BlasterBullet")
        {
            //Уничтожаем родительский объект со спецэффектами
            GameObject Explode = Instantiate(Explode_prefab, transform.position, Random.rotation); //Создаём клон объекта с собственными координатами и поворотом
            float explode_size = this.gameObject.GetComponent<MeshRenderer>().bounds.extents.magnitude;
            Explode.transform.localScale = new Vector3(explode_size, explode_size, explode_size);
            AudioSource explode_audio = Explode.gameObject.GetComponent<AudioSource>();
            explode_audio.pitch = 1f / (Explode.transform.localScale.magnitude / 5f); //Задаём высоту звука взрыва в соответствии с размером объекта 
            explode_audio.volume = 1f / (explode_audio.pitch * 5f);
            Destroy(Explode.gameObject, 5);
            Destroy(this.gameObject, 0);
        }
    }
}
