﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid_Damage : MonoBehaviour
{
    public GameObject asteroid_spawner1, asteroid_spawner2;
    List<GameObject> asteroid_spawner_list1, asteroid_spawner_list2;
    public GameObject Explode_prefab;
    Rigidbody AsteroidBody;

    // Start is called before the first frame update
    void Start()
    {
        if (asteroid_spawner1 != null) asteroid_spawner_list1 = asteroid_spawner1.GetComponent<Classic_Asteroid_Spawner>().Asteroids;
        if (asteroid_spawner2 != null) asteroid_spawner_list2 = asteroid_spawner2.GetComponent<Classic_Asteroid_Spawner>().Asteroids;
        
        AsteroidBody = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void OnCollisionEnter(Collision other)
    {
        //Разбиваем астеройд если он столкнулся с пулей
        if (other.gameObject.tag == "BlasterBullet")
        {
            //Если в скрипте указан список для дочерних объектов, то создаём два из них, в противном случае просто уничтожаем свой объект
            if (asteroid_spawner1 != null)
            {
                //Задаем произвольное количество осколков (от 2 до 3х)
                int child_count = Random.Range(2, 3);
                //Создаем дочерние объекты
                if (child_count >= 1)
                {
                    GameObject created_obj_1 = Instantiate(asteroid_spawner_list1[Random.Range(0, asteroid_spawner_list1.Count -1)], transform.position, Random.rotation) as GameObject;
                    created_obj_1.GetComponent<Rigidbody>().velocity = AsteroidBody.velocity;
                    created_obj_1.GetComponent<Rigidbody>().AddForce(created_obj_1.transform.forward * Random.Range(0.1f, 2f), ForceMode.VelocityChange);
                }
                if (child_count >= 2)
                {
                    GameObject created_obj_2 = Instantiate(asteroid_spawner_list1[Random.Range(0, asteroid_spawner_list1.Count -1)], transform.position, Random.rotation) as GameObject;
                    created_obj_2.GetComponent<Rigidbody>().velocity = AsteroidBody.velocity;
                    created_obj_2.GetComponent<Rigidbody>().AddForce(created_obj_2.transform.forward * Random.Range(0.1f, 2f), ForceMode.VelocityChange);
                }
                if (child_count >= 3)
                {
                    GameObject created_obj_3 = Instantiate(asteroid_spawner_list1[Random.Range(0, asteroid_spawner_list1.Count - 1)], transform.position, Random.rotation) as GameObject;
                    created_obj_3.GetComponent<Rigidbody>().velocity = AsteroidBody.velocity;
                    created_obj_3.GetComponent<Rigidbody>().AddForce(created_obj_3.transform.forward * Random.Range(0.1f, 2f), ForceMode.VelocityChange);
                }
            }
            if (asteroid_spawner2 != null)
            {
                //Задаем произвольное количество осколков (от 0 до 2х)
                int child_count = Random.Range(0, 2);
                //Создаем дочерние объекты
                if (child_count >= 1)
                {
                    GameObject created_obj_1 = Instantiate(asteroid_spawner_list2[Random.Range(0, asteroid_spawner_list2.Count - 1)], transform.position, Random.rotation) as GameObject;
                    created_obj_1.GetComponent<Rigidbody>().velocity = AsteroidBody.velocity;
                    created_obj_1.GetComponent<Rigidbody>().AddForce(created_obj_1.transform.forward * Random.Range(0.1f, 2f), ForceMode.VelocityChange);
                }
                if (child_count >= 2)
                {
                    GameObject created_obj_2 = Instantiate(asteroid_spawner_list2[Random.Range(0, asteroid_spawner_list2.Count - 1)], transform.position, Random.rotation) as GameObject;
                    created_obj_2.GetComponent<Rigidbody>().velocity = AsteroidBody.velocity;
                    created_obj_2.GetComponent<Rigidbody>().AddForce(created_obj_2.transform.forward * Random.Range(0.1f, 2f), ForceMode.VelocityChange);
                }
            }

            //Уничтожаем родительский объект со спецэффектами
            GameObject Explode = Instantiate(Explode_prefab, transform.position, Random.rotation); //Создаём клон объекта с собственными координатами и поворотом
            float explode_size = this.gameObject.GetComponent<MeshRenderer>().bounds.extents.magnitude / 3;
            Explode.transform.localScale = new Vector3(explode_size, explode_size, explode_size);
            AudioSource explode_audio = Explode.gameObject.GetComponent<AudioSource>();
            explode_audio.pitch = 1f / (Explode.transform.localScale.magnitude / 5f); //Задаём высоту звука взрыва в соответствии с размером объекта 
            explode_audio.volume = 1f / (explode_audio.pitch * 5f);
            Destroy(Explode.gameObject, 5);
            Destroy(this.gameObject, 0);
        }
        //else

    }

    void FixedUpdate()
    {
        //Устанавливаем максимальную скорость астеройдов, чтобы они не разгонялись больше положенного
        if (AsteroidBody.velocity.magnitude > 50f) AsteroidBody.velocity = AsteroidBody.velocity.normalized * 50f;
        if ((transform.position.y > 1f) || (transform.position.y < -1f)) transform.position = new Vector3(transform.position.x, 0f, transform.position.z);
    }
}

