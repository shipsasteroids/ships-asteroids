﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JewelsBank : MonoBehaviour
{
    Text JewelsSign;

    // Start is called before the first frame update
    void Start()
    {
        JewelsSign = this.gameObject.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (JewelsSign.text != "$" + PlayerPrefs.GetInt("Jewels").ToString()) JewelsSign.text = "$" + PlayerPrefs.GetInt("Jewels").ToString();
    }
}
