﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipStatus : MonoBehaviour
{
    public int Price = 10000; //Цена корабля
    public float Health = 100;
    public Slider HealthBar;
    //public ParticleSystem Fire1, Fire2, Fire3, Fire4, Fire5, Fire6;
    public GameObject Fire1, Fire2, Fire3, Fire4, Fire5;
    public GameObject Explode_prefab;
    public GameObject GameOverUILayer;
    GameObject Explode;
    Rigidbody ShipBody;
    Vector3 Y_Correction;

    // Start is called before the first frame update
    void Start()
    {
        Fire1.SetActive(false);
        Fire2.SetActive(false);
        Fire3.SetActive(false);
        Fire4.SetActive(false);
        Fire5.SetActive(false);
        ShipBody = this.GetComponent<Rigidbody>();
    }

    public void OnCollisionEnter(Collision other)
    {
        //Если сталкиваемся с астеройдом
        if (other.gameObject.tag == "Asteroid")
        {
            //Считаем какой урон получен вычитая вектор собственной скорости с вектором скорости объекта столкновения и умножая на соотношение масс
            float MassPercent = other.gameObject.GetComponent<Rigidbody>().mass / this.gameObject.GetComponent<Rigidbody>().mass;
            float Damage = (other.gameObject.GetComponent<Rigidbody>().velocity - this.gameObject.GetComponent<Rigidbody>().velocity).magnitude * (MassPercent) * 0.3f;
            Health = Health - Damage;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Включаем эффекты в зависимости от здоровья корабря
        if (Health <= 80) Fire1.SetActive(true); else Fire1.SetActive(false);
        if (Health <= 60) Fire2.SetActive(true); else Fire2.SetActive(false);
        if (Health <= 40) Fire3.SetActive(true); else Fire3.SetActive(false);
        if (Health <= 20) Fire4.SetActive(true); else Fire4.SetActive(false);
        if (Health <= 10) Fire5.SetActive(true); else Fire5.SetActive(false);
        //Уничтожаем корабль, если здоровье кончилось
        if ((Health <= 0) && (Explode == null))
        {
            Explode = Instantiate(Explode_prefab, transform.position, transform.rotation); //Создаём клон объекта с собственными координатами и поворотом
            Explode.transform.localScale = new Vector3(10,10,10);
            //Отклюючаем двигатели при разрушении
            this.gameObject.GetComponent<Classic_Ship_Control>().EngineRear.SetActive(false);
            this.gameObject.GetComponent<Classic_Ship_Control>().EngineLeft.SetActive(false);
            this.gameObject.GetComponent<Classic_Ship_Control>().EngineRight.SetActive(false);
            this.gameObject.GetComponent<Classic_Ship_Control>().enabled = false;
            //В случае уничтожения активируем экран game over
            if (GameOverUILayer.activeInHierarchy != true) GameOverUILayer.SetActive(true);
            Destroy(this.gameObject);
        }
        //Включаем регенирацию
        if ((Health > 0) && (Health < 100)) Health = Health + 0.01f;
        if (Health < 0) Health = 0; if (Health > 100) Health = 100;

        //Выводим показатели на UI
        if (HealthBar.value != Health) HealthBar.value = Health;
    }

    //Устанавливаем максимальную скорость и выравниваем корабль по высоте, чтобы он не выходил из двухмерной плоскости
    private void FixedUpdate()
    {
        if ((transform.position.y > 1f) || (transform.position.y < -1f)) transform.position = new Vector3(transform.position.x, 0f, transform.position.z);
        if (ShipBody.velocity.magnitude > 100f) ShipBody.velocity = ShipBody.velocity.normalized * 100f;
    }
}
