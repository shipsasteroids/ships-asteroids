﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Monetization;

public class AdsManager : MonoBehaviour
{
    public bool TestMode = true;

#if UNITY_IOS
        private string gameId = "3301589";
#elif UNITY_ANDROID
        private string gameId = "3301588";
#elif UNITY_STANDALONE
        private string gameId = "0";
#elif UNITY_WEBGL
        private string gameId = "0";
#elif UNITY_WSA
        private string gameId = "0";
#endif

    //Отдельно инициализируем рекламный движок, чтобы он был готов заранее
    void Start()
    {
        //if ((Monetization.isSupported) && (gameId != "0"))
        {
            Monetization.Initialize(gameId, TestMode);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
