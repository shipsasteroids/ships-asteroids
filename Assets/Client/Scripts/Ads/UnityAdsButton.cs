﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Monetization;

[RequireComponent(typeof(Button))]
public class UnityAdsButton : MonoBehaviour
{

    public string placementId = "rewardedVideo";
    public int RewardValue = 10000;
    public bool TestMode = true;
    private Button adButton;

#if UNITY_IOS
        private string gameId = "3301589";
#elif UNITY_ANDROID
        private string gameId = "3301588";
#elif UNITY_STANDALONE
        private string gameId = "0";
#elif UNITY_WEBGL
        private string gameId = "0";
#elif UNITY_WSA
    private string gameId = "0";
#endif

    void Start()
    {
        adButton = GetComponent<Button>();
        if (adButton)
        {
            adButton.onClick.AddListener(ShowAd);
        }

        //Инициализируем повторно, если не было инициализировано ранее
        if ((Monetization.isSupported) && (!Monetization.isInitialized) && (gameId != "0"))
        {
            Monetization.Initialize(gameId, TestMode);
        }
    }

    void Update()
    {
        if (adButton)
        {
            adButton.interactable = Monetization.IsReady(placementId);
        }
    }

    void ShowAd()
    {
        ShowAdCallbacks options = new ShowAdCallbacks();
        options.finishCallback = HandleShowResult;
        ShowAdPlacementContent ad = Monetization.GetPlacementContent(placementId) as ShowAdPlacementContent;
        ad.Show(options);
    }

    void HandleShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            // Reward the player
            PlayerPrefs.SetInt("Jewels", (PlayerPrefs.GetInt("Jewels") + RewardValue));
        }
        else if (result == ShowResult.Skipped)
        {
            Debug.LogWarning("The player skipped the video - DO NOT REWARD!");
        }
        else if (result == ShowResult.Failed)
        {
            Debug.LogError("Video failed to show");
        }
    }
}