﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Resources : MonoBehaviour
{
    public int energyEssence;
    public int metalRes;
    public int techPoint;
    public GameObject ResourceSource;
    public GameObject ResourceDestination;


    public void UpdateEnergyAfterKill()
    {
        //Debug.Log(ResourceSource.name);
        //Debug.Log(ResourceDestination.name);

        if ((ResourceSource != null) && (ResourceDestination != null))
        {
            ResourceDestination.gameObject.GetComponent<Resources>().energyEssence = ResourceDestination.gameObject.GetComponent<Resources>().energyEssence + ResourceSource.gameObject.GetComponent<Resources>().energyEssence;
            ResourceSource.gameObject.GetComponent<Resources>().techPoint = 0;
        }
    }

    public void UpdateMetalAfterKill()
    {
        //Debug.Log(ResourceSource.name);
        //Debug.Log(ResourceDestination.name);
        if ((ResourceSource != null) && (ResourceDestination != null))
        {
            ResourceDestination.gameObject.GetComponent<Resources>().metalRes = ResourceDestination.gameObject.GetComponent<Resources>().metalRes + ResourceSource.gameObject.GetComponent<Resources>().metalRes;
            ResourceSource.gameObject.GetComponent<Resources>().techPoint = 0;
        }
    }

    public void UpdateTechAfterKill()
    {
        //Debug.Log(ResourceSource.name);
        //Debug.Log(ResourceDestination.name);
        if ((ResourceSource != null) && (ResourceDestination != null))
        {
            ResourceDestination.gameObject.GetComponent<Resources>().techPoint = ResourceDestination.gameObject.GetComponent<Resources>().techPoint + ResourceSource.gameObject.GetComponent<Resources>().techPoint;
            ResourceSource.gameObject.GetComponent<Resources>().techPoint = 0;

        }
    }

}


/*
Основная мысль в том что есть обменник ресурсов между Source(обьект который появится после смерти корабля/астероида) и Destination(обьектом игрока или обьектом NPC)
Скрипт нужно навешивать на все обьекты на уровне спауна с каким то дефолтным минимальным значением
При смерти обьекта перевешивать его на те обьекты которые заспаунятся и будут хранить в себе ресурсы пока их не подберут(пока передавать напрямую)
      */