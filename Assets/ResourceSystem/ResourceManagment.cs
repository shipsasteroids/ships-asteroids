﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class ResourceManagment : MonoBehaviour
{
    int shots_fired = 0, shots_hit = 0;
    Text shots_fired_text, shots_hit_text;

    // Start is called before the first frame update
    void Start()
    {
        shots_fired_text = GameObject.Find("ShotsFiredText").GetComponent<Text>();
        shots_hit_text = GameObject.Find("ShotsHitText").GetComponent<Text>();
        UpdateText();
    }

    public void Fire()
    {
        shots_fired++;
        UpdateText();
    }

    public void Hit()
    {
        shots_hit++;
        UpdateText();
    }

    void UpdateText()
    {
        shots_fired_text.text = "Выстрелов сделано: " + shots_fired;
        shots_hit_text.text = "Попаданий в цель: " + shots_hit + "/2000";
    }

    // Update is called once per frame
    void Update()
    {
        if (shots_hit >= 2000)
        {
            PlayerPrefs.SetInt("Shots_fired", shots_fired);
            PlayerPrefs.SetInt("Shots_hit", shots_hit);
            PlayerPrefs.Save();
            SceneManager.LoadScene("Menu");
        }

        /* Сохранение массива
        int[] massive;
        for (int i= 0; i < massive.Length; i++)
        {
            PlayerPrefs.SetInt("Massive_" + i, massive[i]);
        }*/
    }
}
